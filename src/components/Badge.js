import { MDBBadge } from "mdb-react-ui-kit";
import React from "react";

const Badge = ({ children, styleInfo }) => {
  const colorkey = {
    Fashion: "primary",
    Travel: "success",
    Fitness: "danger",
    Food: "warning",
    Teach: "info",
    Sports: "dark",
  };
  return (
    <h5 style={styleInfo}>

      <MDBBadge color={colorkey[children]}>

                {children}

      </MDBBadge>
    </h5>
  );
};

export default Badge;
